from grid import *

g = Grid(50, 50)


def rasterline(x1, y1, x2, y2):
    dx = abs(x2 - x1) # Calculate dx
    dy = abs(y2 - y1) # Calculate dy
    slope = dy / float(dx) 

    x, y = x1, y1

    # Check if slope > 1
    # then interchange x and y
    if slope > 1:
        dx, dy = dy, dx
        x, y = y, x
        x1, y1 = y1, x1
        x2, y2 = y2, x2

    p = 2 * dy - dx

    placepixel(x, y)

    for k in range(2, dx):
        if p > 0:
            y = y + 1 if y < y2 else y - 1
            p = p + 2 * (dy - dx)
        else:
            p = p + 2 * dy

        x = x + 1 if x < x2 else x - 1
        placepixel(x, y)


def placepixel(x, y):
    g.addPoint(x, y)


def main():
    # rasterline(x1, y1, x2, y2)
    rasterline(17, 26, 39, 40)
    rasterline(39, 40, 17, 26)
    g.draw()


if __name__ == "__main__":
    main()
